# tags (before "\t") and build arguments (after "\t", separated by " " which cannot be utilised otherwise) of the Docker image variants to build
# for versions, see https://github.com/apache/incubator-livy/releases
# Spark 1.6
#0.5.0-spark1.6-hadoop2.6	FROM_TAG=1.6.3-hadoop2.6 LIVY_NAME=livy LIVY_VERSION=0.5.0-incubating
# Spark 2.0
#0.5.0-spark2.0-hadoop3.2	FROM_TAG=2.0.2-hadoop3.2 LIVY_NAME=livy LIVY_VERSION=0.5.0-incubating
# Spark 2.1
#0.5.0-spark2.1-hadoop3.2	FROM_TAG=2.1.3-hadoop3.2 LIVY_NAME=livy LIVY_VERSION=0.5.0-incubating
# Spark 2.2
#0.5.0-spark2.2-hadoop3.2	FROM_TAG=2.2.3-hadoop3.2 LIVY_NAME=livy LIVY_VERSION=0.5.0-incubating
# Spark 2.3
0.6.0-spark2.3-hadoop3.2	FROM_TAG=2.3.4-hadoop3.2 LIVY_NAME=apache-livy LIVY_VERSION=0.6.0-incubating
# Spark 2.4
0.6.0-spark2.4-hadoop3.2	FROM_TAG=2.4.4-hadoop3.2 LIVY_NAME=apache-livy LIVY_VERSION=0.6.0-incubating
