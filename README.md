# Apache Livy Docker Image

[![pipeline status](https://gitlab.com/rychly-edu/docker/docker-livy/badges/master/pipeline.svg)](https://gitlab.com/rychly-edu/docker/docker-livy/commits/master)
[![coverage report](https://gitlab.com/rychly-edu/docker/docker-livy/badges/master/coverage.svg)](https://gitlab.com/rychly-edu/docker/docker-livy/commits/master)

The image is based on [rychly-docker/docker-spark](/rychly-edu/docker/docker-spark).
The version of the base image can be restricted on build by the `FROM_TAG` build argument.

## Build

### The Latest Version by Docker

~~~sh
docker build --pull -t "registry.gitlab.com/rychly-edu/docker/docker-livy:latest" .
~~~

### All Versions by the Build Script

~~~sh
./build.sh --build "registry.gitlab.com/rychly-edu/docker/docker-livy" "latest"
~~~

For the list of versions to build see [docker-tags.txt file](docker-tags.txt).

## Run by Docker-Compose

If the `RECOVERY_DIR` is set the YARN scheduler is required.

### YARN Scheduler

See [docker-compose.yml](docker-compose.yml).

### Spark Master Scheduler

See [docker-compose-sparkmaster.yml](docker-compose-sparkmaster.yml).
